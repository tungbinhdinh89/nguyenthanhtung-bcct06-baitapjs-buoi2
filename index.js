// Bài tập 01
//Calculate Total Salary
function calcSalary() {
  var wordDays = document.getElementById('wordDays').value;

  var total = wordDays * 100000;
  document.getElementById('salary').style.display = 'block';
  document.getElementById('totalSalary').innerHTML = total;
}

//click to call function
document.getElementById('calculate').onclick = function () {
  calcSalary();
};

// Bài 02
//Calculate Averay of five number
function calcAveray() {
  var num1 = document.getElementById('num1').value;
  var num2 = document.getElementById('num2').value;
  var num3 = document.getElementById('num3').value;
  var num4 = document.getElementById('num4').value;
  var num5 = document.getElementById('num5').value;

  var total =
    (parseInt(num1) +
      parseInt(num2) +
      parseInt(num3) +
      parseInt(num4) +
      parseInt(num5)) /
    5;

  document.getElementById('averay').innerHTML = total;
}

//click to call function
document.getElementById('calcAveray').onclick = function () {
  calcAveray();
};

// Bài 03
//Change from USD to VND
function usdToVND() {
  var change = document.getElementById('change').value;

  var total = change * 23500;
  document.getElementById('usdToVND').innerHTML = total;
}

//click to call function
document.getElementById('changeUSD').onclick = function () {
  usdToVND();
};

// Bài 04
//Calculator Area and Perimeter of Rectangle
function areaAndPerimeter() {
  var lengthRectangle = document.getElementById('lengthRectangle').value;
  var widthRectangle = document.getElementById('widthRectangle').value;

  var area = lengthRectangle * widthRectangle;
  var perimeter = (parseInt(lengthRectangle) + parseInt(widthRectangle)) * 2;
  document.getElementById('areRectangle').innerHTML = area;
  document.getElementById('perimeterRectangle').innerHTML = perimeter;
}

//click to call function
document.getElementById('areaAndPerimeter').onclick = function () {
  areaAndPerimeter();
};

// Bài 05

function totalNumber() {
  var number = document.getElementById('inputNumber').value;

  var result = (number % 10) + Math.floor(number / 10);

  document.getElementById('result').innerHTML = result;
}

//click to call function
document.getElementById('totalNumber').onclick = function () {
  totalNumber();
};
